# Scheduler for linear actuator

Programmet `testrig.py` kjører i en uendelig løkke og sjekker med jevnlige mellomrom filen `job.xml` og utfører kommandoene i filen ved det spesifiserte tidspunktet. Hvis det er to set med kommandoer som er satt til å kjøre i oeverlappende tidsrom er det kun garanti for at den første blir kjørt. Kommandoer med dato vil kun bli kjørt en gang og deretter slettet. Kmmandoer med bare tidspunkt vil bli kjørt hver dag og ikke slettet. Hver linje med kommandoer er blokkende. Programmet bruker biblioteket [grbl](). På wiki sidene finnes det instruks for oppkobling og info om nyttige kommandoer.  


### Motor kommandoer
    @current [value]
    @rpm [value]
    @duty [value]

Current er gitt i mA

Example:

    @current 1000

    @rpm 200

    @duty 0.5


### Stepper kommandoer
For stepper motoren brukes vanlig g-kode. For lineær aktutaoren er null definert som øverst ved endestoppbryteren. Lengden på lineæaktuatoren som kan brukes er `585 mm`. Programmet sjekker ikke om det er en gyldig kommando, men det er ikke mulig å kjøre motoren for langt. 

### Setup

Python bibliotekene kan installeres med pipenv. 
Hvis samme bibliotek for styring av VESC skal brukes senere er det nødvendig å klone repo fra [Github](https://github.com/LiamBindle/PyVESC.git) og installeres (`python setup.py install`). Bibliotektet avhenger også av PyCRC noe som ikke er samme pakke som blir installert via pip. Den rette PyCRC finnes [her](https://github.com/alexbutirskiy/PyCRC).

Kamera startes ved å kjøre `camera_cmd.sh` og python programmet som styrer testriggen kjøres med `python tesrig.py`. Alt kan legges til som [systemd](https://www.linode.com/docs/quick-answers/linux/start-service-at-boot/) "tjenester". Dette er gjort på Pi rpi1. 
Tjenestene heter `testrig_motor.service` og `testrig_cam.service`. De kan startes og stoppes med `sudo systemctl start [SERVICE]` og  `sudo systemctl stop [SERVICE]`. For å sjekke om en tjeneste kjører: `sudo systemctl stop [SERVICE]`. For å kjøre en tjeneste veed oppstart `sudo systemctl enable [SERVICE]` og `sudo systemctl disable [SERVICE]` for å fjerne fra oppstart.

### Kamera:

    [Unit]
    Description=Camera service for test rig.

    [Service]
    Type=simple
    User=pi
    ExecStart=/bin/bash /usr/bin/TestRig/camera_cmd.sh

    [Install]
    WantedBy=multi-user.target

### Motorer:
        
    [Unit]
    Description=Service for linear actuator and motor brush for test rig.

    [Service]
    Type=simple
    User=pi
    WorkingDirectory=/home/pi/TestRig/
    ExecStart=/home/pi/.local/bin/pipenv run python3 /home/pi/TestRig/testrig.py

    [Install]
    WantedBy=multi-user.target


### Innstillinger
Kamera instillinger gjøres i `camera_cmd.sh`. Der settes også IP til mottakeren av kamera feeden. 
Grbl programmet som kjører på arduino er kompilert med header filen som ligger i grbl_config mappen. I tillegg er programmet grbl konfigurert med disse innstillingene:


    $0=10
    $1=25
    $2=0
    $3=0
    $4=0
    $5=0
    $6=0
    $10=1
    $11=0.010
    $12=0.002
    $13=0
    $20=1
    $21=0
    $22=1
    $23=1
    $24=1000.000
    $25=500.000
    $26=250
    $27=10.000
    $30=1000
    $31=0
    $32=0
    $100=162.000
    $101=250.000
    $102=250.000
    $110=500.000
    $111=500.000
    $112=500.000
    $120=10.000
    $121=10.000
    $122=10.000
    $130=650.000
    $131=200.000
    $132=200.000

    
# linear.py
Filen `linear.py` inneholder en del nyttige funskjoner som er brukt under konfigurering og testing av riggen. Må kjøres med Python3 og starter ett interaktivt shell. 