import sys
import serial
import time
from class_cli import CLI


# Max Vertical Speed: 88mm/s = 20 000 mm/minute
# G94 sets the speed to units per minute
# G21 sets the units to millimeters
# G90 is absolute distance mode
# G4 is delay
cli = CLI()
MAX_FEED_RATE = 20000

@cli.Program()
class LinearActuator:

    @cli.Operation()
    def init(self, port, length=100):
        """Initializes the driver

        Args:
            port (int): What com port to use
            length (int, optional): The length of the actuator. Defaults to 1.
        """
        self.driver = serial.Serial(str(port), 115200)
        # Wake up grbl
        self.driver.write(bytes("\r\n\r\n", "ascii"))
        time.sleep(1)   # Wait for grbl to initialize 
        self.driver.flushInput()  # Flush startup text in serial input
        self.mode = "G1" # G01 = slow, G00 = Fast as possible
        self._pos = 0
        self.length = length
        self._feed_rate = "F1000" # max = F20000
        print("init finished")

    @cli.Operation()
    def execute_line(self, line):
        if self.mode == "G1" and self._feed_rate == "":
            print("Set feed rate")
            return
        if "\n" not in line:
            line += " \n"

        self.driver.write(bytes(line,"ascii"))
        time.sleep(1)
        num_bytes = self.driver.in_waiting
        response = self.driver.read(num_bytes)
        print(response.decode("ascii"))

    @cli.Operation()
    def bottom(self):
        """Drive to the bottom
        """
        line = self.mode +" "+ "X" +str(self.length) +self._feed_rate+" \n"
        self.execute_line(line)
        self._pos = self.length

    @cli.Operation()
    def top(self):
        """Drive to the top
        """
        line = self.mode +" " + "X0" +self._feed_rate+" \n"
        self.execute_line(line)
        self._pos = 0

    @cli.Operation()
    def go_to(self, pos:float):
        """Go to a specific position. Unit in millimeters

        Args:
            pos (float): End position.
        """
        if pos > self.length or pos < 0:
            print(f"Invalid position {pos} m.")
            return
        line = self.mode +" "+ "X" +str(pos) +self._feed_rate+" \n"
        self.execute_line(line)
        self._pos = pos

    @cli.Operation()
    def feed_rate(self, rate:float):
        """Sets the feed rate

        Args:
            rate (float): Feed rate
        """
        if rate < MAX_FEED_RATE and rate > 0:
            self._feed_rate = "F"+str(rate)
        else:
            print("Invalid feed rate")

    @cli.Operation()
    def soft_reset(self):
        """Sends special character to the arduino for soft reseet.
        """
        self.driver.write(bytes("\x18 \n","ascii"))
        time.sleep(1)
        self.driver.reset_input_buffer()

    @cli.Operation()
    def get_parser_state(self):
        try:
            self.driver.write(bytes("$G \n", "ascii"))
            num_bytes = self.driver.in_waiting
            time.sleep(1)
            print(self.driver.read(num_bytes))
        except ConnectionResetError:
            print("Try again...")

    @cli.Operation()
    def get_status(self):
        try:
            self.driver.write(bytes("?", "ascii"))
            time.sleep(1)
            num_bytes = self.driver.in_waiting
            print(self.driver.read(num_bytes).decode("ascii"))
        except ConnectionResetError:
            print("Try again...")

    @cli.Operation()
    def jog_down(self, step:int):
        line = "G91 " +self.mode+ " X"+ str(step) +" "+ self._feed_rate +" \n G90 \n"
        self.execute_line(line)

    @cli.Operation()
    def jog_up(self, step:int):
        self.jog_down(-step)

    @cli.Operation()
    def homing(self):
        line = "$H \n"
        self.execute_line(line)

    @cli.Operation()
    def kill_alarm(self):
        line = "$X \n"
        self.execute_line(line)

if __name__ == "__main__":
    LinearActuator().CLI.main()
