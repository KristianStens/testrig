import datetime
import schedule
import serial
import time
import xml.etree.ElementTree as ET
from pyvesc import VESC
import sys

CHECK_INTERVAL = 5

MAX_MILLIAMPS = 1000 # mA
MIN_MILLIAMPS = 0
MAX_RPM = 100
MIN_RPM = 0
MAX_DUTY = 1
MIN_DUTY = 0
MAX_LENGTH = 585

# TODO: Fjerne sekunder?

class TestRig:

    def __init__(self, grbl_port, motor_port, job_file="job.xml", log_file="log.xml"):
        if sys.platform == "linux":
            if "/dev/" not in motor_port:
                motor_port = "/dev/" + motor_port
            if "/dev/" not in grbl_port:
                grbl_port = "/dev/" + grbl_port
        self.job_file = job_file
        self.log_file = log_file
        self.driver = serial.Serial(grbl_port, 115200)
        # Wake up grbl
        self.driver.write(bytes("\r\n\r\n", "ascii"))
        time.sleep(5)   # Wait for grbl to initialize 
        self.driver.reset_input_buffer()  # Flush startup text in serial input
        self.motor = VESC(serial_port=motor_port)

    def is_running(self):
        self.driver.write(bytes("?", "ascii"))
        # Blocking
        status = self.driver.readline().decode("ascii")
        if status.find("Idle") > -1:
            # Found "Idle" in status
            return False
        else:
            return True

    def wait_until_done(self):
        """Blocking until the rig has executed g-code.
        """
        while True:
            time.sleep(1)
            # Wait for the job to finish
            if not self.is_running():
                break

 
    def execute_commands(self, commands):
        """Executes the g-code passed as argument

        Args:
            commands (string): String with commands
        """
        # Split the text into a list of lines
        lines = commands.split("\n")[:-1]
        start_time = datetime.datetime.now()
        for line in lines:
            # Motor command
            line = line.strip() # Remove spaces
            if line[0] == "@":
                # run motor
                mode, value = line[1:].split()
                value = int(value) if value.isdigit() else 0  # "Safe" cast
                self.run_motor(mode, value)

            # G-code
            elif line[0].upper() == "G":

                start_of_length = line.find("X") # Don't execute if outside bounds. To avoid alarm.
                if start_of_length > -1:
                    length = line[start_of_length+1:].split(" ")[0]
                    length = int(length) if length.isdigit() else 0
                    if MAX_LENGTH < length or length < 0:
                        continue 

                self.driver.write(bytes(line +' \n' , "ascii")) # Send g-code block to grbl
                num_bytes = self.driver.in_waiting
                response = self.driver.read(num_bytes).decode("ascii").strip()
                # Log error code
                if response !=  "ok" and response.find("Grbl 1.1h ['$' for help]") > 0:
                    self.log_invalid_commands(response, line)
                # wait for g-code to be finished
                self.wait_until_done()
                time.sleep(0.5) # If too many lines sent at once, the arduino will discard them 

        self.driver.write(bytes("G1 X10 \n" , "ascii"))  # Always go to "home" position 
        time.sleep(0.5)
        self.driver.reset_input_buffer() # Cleanup
        self.stop_motor()
        self.log_job(start_time, commands)
            
    def schedule_jobs(self):
        """Parses the xml file with jobs and schdules the g-code for executing.
        """
        tree = ET.parse(self.job_file)
        root = tree.getroot()
        elements = []
        # Sort after ascending time
        for elem in root.iter("executionTime"):
            elements.append((elem.attrib["time"], elem))
        elements.sort()

        for _, elem in elements:
            # Extract timestamps and gcode
            commands = elem.find("commands").text
            timestamp = elem.attrib["time"].replace(" ", "")

            try:
                timestamp = datetime.datetime.strptime(timestamp, "%d/%m/%Y,%H:%M:%S")
                if timestamp > datetime.datetime.now() and timestamp < datetime.datetime.now() + datetime.timedelta(seconds=CHECK_INTERVAL):
                    root.remove(elem)  # remove element, date specific element
                    tree.write(self.job_file) # Write to xml file
                    self.execute_commands(commands)  # Run if within specific range
                    return # Executed a job, wait for next check
                else:
                    continue # Not within window
            except ValueError:
                try:
                    timestamp = datetime.datetime.strptime(timestamp, "%H:%M:%S").time()
                    if timestamp > datetime.datetime.now().time() and timestamp < (datetime.datetime.now() + datetime.timedelta(seconds=CHECK_INTERVAL)).time():
                        self.execute_commands(commands) # Run if within specific range
                        return # Executed a job, wait for next check
                except ValueError:
                    pass # Wrong format

    def log_job(self, start_time, commands):
        """Logs what has been done by the rig.

        Args:
            commands (string): String of commands commands
        """
        tree = ET.parse(self.log_file)
        root = tree.getroot()

        start_time = start_time.strftime("%d/%m/%Y, %H:%M:%S")
        end_time = datetime.datetime.now()
        end_time = end_time.strftime("%d/%m/%Y, %H:%M:%S")

        exec_elemnet = ET.Element("executionTime")
        exec_elemnet.set("startTime", start_time)
        exec_elemnet.set("endTime", end_time)

        commands_element = ET.SubElement(exec_elemnet, "commands")
        commands_element.text = commands
        root.append(exec_elemnet)
        tree.write(self.log_file)

    def log_invalid_commands(self, error, commands):
        tree = ET.parse(self.log_file)
        root = tree.getroot()
        log_time = datetime.datetime.now()
        log_time = log_time.strftime("%d/%m/%Y, %H:%M:%S")

        exec_elemnet = ET.Element("executionTime")
        exec_elemnet.set("time", log_time)

        commands_element = ET.SubElement(exec_elemnet, "commands")
        commands_element.text = commands
        
        error_element = ET.SubElement(exec_elemnet, "error_code")
        error_element.text = error

        root.append(exec_elemnet)
        tree.write(self.log_file)

    def run_motor(self, mode, value):
        if mode == "current" and MIN_MILLIAMPS <= value <= MAX_MILLIAMPS:
            self.motor.set_current(value)

        elif mode == "rpm" and MIN_RPM <= value <= MAX_RPM:
            self.motor.set_rpm(value)
        
        elif mode == "duty" and MIN_DUTY <= value <= MAX_DUTY:
            self.motor.set_duty(value)
        else:
            print("Invalid command")

    def stop_motor(self):
        self.motor.set_current(0)

    def kill_alarm(self):
        self.driver.write(bytes("\x18 \n","ascii"))
        self.driver.write(bytes("$X \n", "ascii"))

    def calibrate(self):
        self.driver.write(bytes("$H \n", "ascii"))
        self.wait_until_done()
        self.driver.read(self.driver.in_waiting) # Flush


if __name__ == "__main__":
    rig = TestRig("ttyACM0", "ttyACM1") # ports on the pi 
    rig.calibrate()
    schedule.every(CHECK_INTERVAL).seconds.do(rig.schedule_jobs) # Check for updates to the script

    while True:
        schedule.run_pending()
