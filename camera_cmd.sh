#!/usr/bin/env bash
# Parameters
WIDTH=1024
HEIGHT=576
FPS=15
IP="192.168.205.190"
# Command
gst-launch-1.0 -v v4l2src device=/dev/video0 ! "video/x-raw, format=YUY2, width=${WIDTH},height=${HEIGHT}, framerate=${FPS}/1" ! videoconvert ! queue ! omxh264enc ! queue ! rtph264pay name=pay0 pt=96 config-interval=2 ! udpsink host=${IP} port=5001