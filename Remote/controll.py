from class_cli import CLI
import time

cli = CLI()

class TestRigRemote:

    @cli.Operation()
    def start_cam():
        pass

    @cli.Operation()
    def stop_cam():
        pass
    
    @cli.Operation()
    def start_rig():
        pass

    @cli.Operation()
    def stop_rig():
        pass
    
    @cli.Operation()
    def reboot():
        pass