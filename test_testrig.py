import xml.etree.ElementTree as ET
import datetime
import random

def make_job(commands, date_time, file="job.xml"):
    """Adds a set of commands to the given xml file for scheduling.

    Args:
        commands (list): list of commands as strings
        date_time (datetime): A datetime object specifying when to execute the commands
        file (string): Path to the xml file
    """
    tree = ET.parse(file)
    root = tree.getroot()

    exec_time = date_time.strftime("%d/%m/%Y, %H:%M:%S")
    exec_elemnet = ET.Element("executionTime")
    exec_elemnet.set("time", exec_time)

    commands_element = ET.SubElement(exec_elemnet, "commands")
    commands_element.text = commands

    root.append(exec_elemnet)
    tree.write(file)


# For testing
def _make_line(x, rate=1000):
    return f"G1 X{str(x)} F{str(rate)} \n"


def _clear_job():
    f = open("job.xml", 'w')
    f.write("<Job></Job>")
    f.close()

# Lager test data
for i in range(10,300,30):
    commands = [_make_line((i%2)*80) for i in range(10)]
    commands.insert(3, "@current 500 \n")
    commands.insert(5, "@rpm 100 \n")
    commands = "".join(commands)
    t = datetime.datetime.now() + datetime.timedelta(seconds=i)
    make_job(commands, t, "job.xml")