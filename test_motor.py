from pyvesc import VESC
import time
import random
import sys

# a function to show how to use the class as a static object.
def run_motor_rpm(port, value): # requires hall sensor
    try:
        value = int(value)
    except:
        return
    with VESC(serial_port=port) as motor:
        print("Firmware: ", motor.get_firmware_version())
        motor.set_rpm(value)

        # run motor and print out rpm
        for i in range(4):
            time.sleep(0.1)
            print(motor.get_measurements().rpm)
        motor.set_rpm(0)


def run_motor_duty(port, value):
    try:
        value = int(value)
    except:
        return
    with VESC(serial_port=port) as motor:
        print("Firmware: ", motor.get_firmware_version())
        motor.set_duty_cycle(value)

        # run motor and print out rpm
        for i in range(30):
            time.sleep(0.1)
            print(motor.get_measurements().rpm)
        motor.set_duty_cycle(0)


def run_motor_current(port, value):
    try:
        value = int(value)
    except:
        return
    with VESC(serial_port=port) as motor:
        print("Firmware: ", motor.get_firmware_version())

        motor.set_current(value) # mA

        # run motor and print out rpm
        for i in range(2):
            time.sleep(0.1)
            print(motor.get_measurements().rpm)
        motor.set_current(0)


if __name__ == '__main__':
    globals()[sys.argv[1]](sys.argv[2], sys.argv[3])